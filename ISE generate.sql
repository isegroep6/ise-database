/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2008                    */
/* Created on:     06/06/2019 14:45:42                          */
/*==============================================================*/
go
use filmcheques
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Address')
            and   type = 'U')
   drop table Address
go

if exists (select 1
            from  sysobjects
           where  id = object_id('BlockReasons')
            and   type = 'U')
   drop table BlockReasons
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Cinema')
            and   type = 'U')
   drop table Cinema
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Employee')
            and   type = 'U')
   drop table Employee
go

if exists (select 1
            from  sysobjects
           where  id = object_id('EmployeePermission')
            and   type = 'U')
   drop table EmployeePermission
go

if exists (select 1
            from  sysobjects
           where  id = object_id('EmployeeRole')
            and   type = 'U')
   drop table EmployeeRole
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Film')
            and   type = 'U')
   drop table Film
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Filmcheque')
            and   type = 'U')
   drop table Filmcheque
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Onlineorder')
            and   type = 'U')
   drop table Onlineorder
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Performance')
            and   type = 'U')
   drop table Performance
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Performanceofcinema')
            and   type = 'U')
   drop table Performanceofcinema
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Permission')
            and   type = 'U')
   drop table Permission
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PermissionOfRole')
            and   type = 'U')
   drop table PermissionOfRole
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Printer')
            and   type = 'U')
   drop table Printer
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Product')
            and   type = 'U')
   drop table Product
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Producttype')
            and   type = 'U')
   drop table Producttype
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Producttypeprice')
            and   type = 'U')
   drop table Producttypeprice
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Producttypesvansalespoint')
            and   type = 'U')
   drop table Producttypesvansalespoint
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Role')
            and   type = 'U')
   drop table Role
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Room')
            and   type = 'U')
   drop table Room
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Salespoint')
            and   type = 'U')
   drop table Salespoint
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Workplace')
            and   type = 'U')
   drop table Workplace
go

if exists (select 1
   from  sysobjects where type = 'D'
   and name = 'DEFAULT_PRINT_STATUS'
   )
   drop default DEFAULT_PRINT_STATUS
go

/*==============================================================*/
/* Default: DEFAULT_PRINT_STATUS                                */
/*==============================================================*/
create default DEFAULT_PRINT_STATUS
    as 'sold'
go

/*==============================================================*/
/* Table: Address                                               */
/*==============================================================*/
create table Address (
   AddressNumber        int                  identity,
   Street               varchar(255)         not null,
   PostalCode           varchar(6)           not null,
   Firstname            varchar(255)         null,
   Lastname             varchar(255)         null,
   City                 varchar(255)         not null,
   Email                varchar(25)          null,
   CompanyName          varchar(255)         null,
   constraint PK_ADDRESS primary key nonclustered (AddressNumber)
)
go

/*==============================================================*/
/* Table: BlockReasons                                          */
/*==============================================================*/
create table BlockReasons (
   Reason               varchar(255)         not null,
   constraint PK_BLOCKREASONS primary key nonclustered (Reason)
)
go

/*==============================================================*/
/* Table: Cinema                                                */
/*==============================================================*/
create table Cinema (
   CinemaNumber         int                  identity,
   EmployeeNumber       int                  not null,
   CinemaName           varchar(255)         not null,
   constraint PK_CINEMA primary key nonclustered (CinemaNumber)
)
go

/*==============================================================*/
/* Index: MANAGER_OF_CINEMA_FK                                  */
/*==============================================================*/
create index MANAGER_OF_CINEMA_FK on Cinema (
EmployeeNumber ASC
)
go

/*==============================================================*/
/* Table: Employee                                              */
/*==============================================================*/
create table Employee (
   EmployeeNumber       int                  identity,
   Email                varchar(25)          not null,
   Password             varchar(255)         not null,
   Status               varchar(255)         not null,
   constraint PK_EMPLOYEE primary key nonclustered (EmployeeNumber)
)
go

/*==============================================================*/
/* Table: EmployeePermission                                    */
/*==============================================================*/
create table EmployeePermission (
   EmployeeNumber       int                  null,
   PermissionName       varchar(255)         null
)
go

/*==============================================================*/
/* Table: EmployeeRole                                          */
/*==============================================================*/
create table EmployeeRole (
   RoleName             varchar(250)         null,
   EmployeeNumber       int                  null
)
go

/*==============================================================*/
/* Table: Film                                                  */
/*==============================================================*/
create table Film (
   FilmNumber           int                  identity,
   Name                 varchar(255)         not null,
   constraint PK_FILM primary key nonclustered (FilmNumber)
)
go

/*==============================================================*/
/* Index: MOVIE_OF_PRODUCTTYPE_FK                               */
/*==============================================================*/
create index MOVIE_OF_PRODUCTTYPE_FK on Film (
FilmNumber ASC
)
go

/*==============================================================*/
/* Table: Filmcheque                                            */
/*==============================================================*/
create table Filmcheque (
   FilmchequeNumber     int                  identity,
   CinemaNumber         int                  not null,
   SalespointNumber     int                  not null,
   BlockedByEmployeeNumber int                  null,
   OrderNumber          int                  null,
   SaleWorkplaceNumber  int                  null,
   SalePrinterNumber    int                  null,
   PrintWorkplaceNumber int                  null,
   PrintStatus          varchar(40)          not null default 'sold',
   ValidMonday          bit                  not null,
   ValidTuesday         bit                  not null,
   ValidWednesday       bit                  not null,
   ValidThursday        bit                  not null,
   ValidFriday          bit                  not null,
   ValidSaturday        bit                  not null,
   ValidSunday          bit                  not null,
   ValidUntil           datetime             null,
   BlockReason          varchar(255)         null,
   Remark               varchar(255)         null,
   constraint PK_FILMCHEQUE primary key nonclustered (FilmchequeNumber)
)
go

/*==============================================================*/
/* Index: FILMCHEQUE_BLOCKED_BY_EMPLOYEE_FK                     */
/*==============================================================*/
create index FILMCHEQUE_BLOCKED_BY_EMPLOYEE_FK on Filmcheque (
BlockedByEmployeeNumber ASC
)
go

/*==============================================================*/
/* Index: PRINT_WORKPLACE_OF_FILMCHEQUE_FK                      */
/*==============================================================*/
create index PRINT_WORKPLACE_OF_FILMCHEQUE_FK on Filmcheque (
PrintWorkplaceNumber ASC
)
go

/*==============================================================*/
/* Index: FILMCHEQUE_VAN_VERKOOPPUNT_FK                         */
/*==============================================================*/
create index FILMCHEQUE_VAN_VERKOOPPUNT_FK on Filmcheque (
CinemaNumber ASC,
SalespointNumber ASC
)
go

/*==============================================================*/
/* Index: FILMCHEQUE_VAN_PRINTER_FK                             */
/*==============================================================*/
create index FILMCHEQUE_VAN_PRINTER_FK on Filmcheque (
SalePrinterNumber ASC
)
go

/*==============================================================*/
/* Index: FILMCHEQUE_VAN_ONLINE_BESTELLING_FK                   */
/*==============================================================*/
create index FILMCHEQUE_VAN_ONLINE_BESTELLING_FK on Filmcheque (
OrderNumber ASC
)
go

/*==============================================================*/
/* Index: SALE_WORKPLACE_VAN_FILMCHEQUE_FK                      */
/*==============================================================*/
create index SALE_WORKPLACE_VAN_FILMCHEQUE_FK on Filmcheque (
SaleWorkplaceNumber ASC
)
go

/*==============================================================*/
/* Index: FILMCHEQUE_BLOCKED_FOR_REASON_FK                      */
/*==============================================================*/
create index FILMCHEQUE_BLOCKED_FOR_REASON_FK on Filmcheque (
BlockReason ASC
)
go

/*==============================================================*/
/* Table: Onlineorder                                           */
/*==============================================================*/
create table Onlineorder (
   OrderNumber          int                  identity,
   ShippingAddressNumber int                  not null,
   BillingAddressNumber int                  not null,
   PersonalMessage      varchar(255)         null,
   CreatedAt            datetime             null default getdate(),
   constraint PK_ONLINEORDER primary key nonclustered (OrderNumber)
)
go

/*==============================================================*/
/* Index: VERZENDADRES_VAN_ONLINE_BESTELLING_FK                 */
/*==============================================================*/
create index VERZENDADRES_VAN_ONLINE_BESTELLING_FK on Onlineorder (
BillingAddressNumber ASC
)
go

/*==============================================================*/
/* Index: FACTUURADRES_VAN_ONLINE_BESTELLING_FK                 */
/*==============================================================*/
create index FACTUURADRES_VAN_ONLINE_BESTELLING_FK on Onlineorder (
ShippingAddressNumber ASC
)
go

/*==============================================================*/
/* Table: Performance                                           */
/*==============================================================*/
create table Performance (
   FilmNumber           int                  not null,
   PerformancePerformanceNumber int                  not null,
   PerformanceRoomNumber int                  not null,
   PerformanceDate      datetime             not null,
   PerformanceTime      datetime             not null,
   constraint PK_PERFORMANCE primary key nonclustered (FilmNumber, PerformancePerformanceNumber)
)
go

/*==============================================================*/
/* Index: FILM_VAN_VOORSTELLING_FK                              */
/*==============================================================*/
create index FILM_VAN_VOORSTELLING_FK on Performance (
FilmNumber ASC
)
go

/*==============================================================*/
/* Index: ROOM_OF_PERFORMANCE_FK                                */
/*==============================================================*/
create index ROOM_OF_PERFORMANCE_FK on Performance (
PerformanceRoomNumber ASC
)
go

/*==============================================================*/
/* Table: Performanceofcinema                                   */
/*==============================================================*/
create table Performanceofcinema (
   FilmNumber           int                  not null,
   PerformanceOfCinemaNumber int                  not null,
   CinemaNumber         int                  not null,
   constraint PK_PERFORMANCEOFCINEMA primary key (FilmNumber, PerformanceOfCinemaNumber, CinemaNumber)
)
go

/*==============================================================*/
/* Index: VOORSTELLING_VAN_BIOSCOOP_FK                          */
/*==============================================================*/
create index VOORSTELLING_VAN_BIOSCOOP_FK on Performanceofcinema (
FilmNumber ASC,
PerformanceOfCinemaNumber ASC
)
go

/*==============================================================*/
/* Index: VOORSTELLING_VAN_BIOSCOOP2_FK                         */
/*==============================================================*/
create index VOORSTELLING_VAN_BIOSCOOP2_FK on Performanceofcinema (
CinemaNumber ASC
)
go

/*==============================================================*/
/* Table: Permission                                            */
/*==============================================================*/
create table Permission (
   PermissionName       varchar(255)         not null,
   constraint PK_PERMISSION primary key nonclustered (PermissionName)
)
go

/*==============================================================*/
/* Table: PermissionOfRole                                      */
/*==============================================================*/
create table PermissionOfRole (
   PermissionName       varchar(255)         null,
   RoleName             varchar(250)         null
)
go

/*==============================================================*/
/* Table: Printer                                               */
/*==============================================================*/
create table Printer (
   PrinterNumber        int                  identity,
   IpAddress            varchar(15)          not null,
   PrintCode            varchar(255)         not null,
   constraint PK_PRINTER primary key nonclustered (PrinterNumber)
)
go

/*==============================================================*/
/* Table: Product                                               */
/*==============================================================*/
create table Product (
   ProductNumber        int                  identity,
   FilmchequeNumber     int                  not null,
   ProductTypeName      varchar(45)          not null,
   RoomNumber           int                  not null,
   Price                money                null,
   Amount               int                  null,
   BarCode              varchar(25)          not null,
   constraint PK_PRODUCT primary key nonclustered (ProductNumber)
)
go

/*==============================================================*/
/* Index: PRODUCTTYPE_VAN_PRODUCT_FK                            */
/*==============================================================*/
create index PRODUCTTYPE_VAN_PRODUCT_FK on Product (
ProductTypeName ASC
)
go

/*==============================================================*/
/* Table: Producttype                                           */
/*==============================================================*/
create table Producttype (
   ProductTypeName      varchar(45)          not null,
   FilmNumber           int                  null,
   constraint PK_PRODUCTTYPE primary key nonclustered (ProductTypeName)
)
go

/*==============================================================*/
/* Table: Producttypeprice                                      */
/*==============================================================*/
create table Producttypeprice (
   ProductTypeName      varchar(45)          not null,
   CinemaNumber         int                  not null,
   Price                money                not null,
   constraint PK_PRODUCTTYPEPRICE primary key (ProductTypeName, CinemaNumber)
)
go

/*==============================================================*/
/* Index: PRODUCTTYPE_IN_PRODUCT_PRICE_FK                       */
/*==============================================================*/
create index PRODUCTTYPE_IN_PRODUCT_PRICE_FK on Producttypeprice (
ProductTypeName ASC
)
go

/*==============================================================*/
/* Index: BIOSCOOP_IN_PRODUCTTYPE_PRICE_FK                      */
/*==============================================================*/
create index BIOSCOOP_IN_PRODUCTTYPE_PRICE_FK on Producttypeprice (
CinemaNumber ASC
)
go

/*==============================================================*/
/* Table: Producttypesvansalespoint                             */
/*==============================================================*/
create table Producttypesvansalespoint (
   ProductypesOfSaleProducttypeName varchar(45)          not null,
   CinemaNumber         int                  not null,
   SalespointNumber     int                  not null,
   constraint PK_PRODUCTTYPESVANSALESPOINT primary key (CinemaNumber, ProductypesOfSaleProducttypeName, SalespointNumber)
)
go

/*==============================================================*/
/* Index: PRODUCTTYPES_VAN_SALESPOINT_FK                        */
/*==============================================================*/
create index PRODUCTTYPES_VAN_SALESPOINT_FK on Producttypesvansalespoint (
ProductypesOfSaleProducttypeName ASC
)
go

/*==============================================================*/
/* Index: PRODUCTTYPES_VAN_SALESPOINT2_FK                       */
/*==============================================================*/
create index PRODUCTTYPES_VAN_SALESPOINT2_FK on Producttypesvansalespoint (
CinemaNumber ASC,
SalespointNumber ASC
)
go

/*==============================================================*/
/* Table: Role                                                  */
/*==============================================================*/
create table Role (
   RoleName             varchar(250)         not null,
   constraint PK_ROLE primary key nonclustered (RoleName)
)
go

/*==============================================================*/
/* Table: Room                                                  */
/*==============================================================*/
create table Room (
   CinemaNumber         int                  not null,
   RoomNumber           int                  not null,
   Name                 varchar(255)         not null,
   constraint PK_ROOM primary key nonclustered (RoomNumber)
)
go

/*==============================================================*/
/* Index: ZAAL_IN_BIOSCOOP_FK                                   */
/*==============================================================*/
create index ZAAL_IN_BIOSCOOP_FK on Room (
CinemaNumber ASC
)
go

/*==============================================================*/
/* Table: Salespoint                                            */
/*==============================================================*/
create table Salespoint (
   CinemaNumber         int                  not null,
   SalespointNumber     int                  identity,
   EmployeeNumber       int                  not null,
   Name                 varchar(255)         not null,
   constraint PK_SALESPOINT primary key nonclustered (CinemaNumber, SalespointNumber)
)
go

/*==============================================================*/
/* Index: VERKOOPPUNT_VAN_BIOSCOOP_FK                           */
/*==============================================================*/
create index VERKOOPPUNT_VAN_BIOSCOOP_FK on Salespoint (
CinemaNumber ASC
)
go

/*==============================================================*/
/* Index: MANAGER_OF_SALESPOINT_FK                              */
/*==============================================================*/
create index MANAGER_OF_SALESPOINT_FK on Salespoint (
EmployeeNumber ASC
)
go

/*==============================================================*/
/* Table: Workplace                                             */
/*==============================================================*/
create table Workplace (
   EmployeeNumber       int                  not null,
   WorkplaceNumber      int                  identity,
   CinemaNumber         int                  not null,
   SalespointNumber     int                  not null,
   WorkplaceName        varchar(255)         not null,
   constraint PK_WORKPLACE primary key nonclustered (WorkplaceNumber)
)
go

/*==============================================================*/
/* Index: EMPLOYEE_OF_WORKPLACE_FK                              */
/*==============================================================*/
create index EMPLOYEE_OF_WORKPLACE_FK on Workplace (
EmployeeNumber ASC
)
go

/*==============================================================*/
/* Index: WORKPLACE_OF_SALESPOINT_FK                            */
/*==============================================================*/
create index WORKPLACE_OF_SALESPOINT_FK on Workplace (
CinemaNumber ASC,
SalespointNumber ASC
)
go

alter table Cinema
   add constraint FK_EMPLOYEE_manages_CINEMA foreign key (EmployeeNumber)
      references Employee (EmployeeNumber)
go

alter table EmployeePermission
   add constraint FK_EMPLOYEE_POE_EMPLO_EMPLOYEE foreign key (EmployeeNumber)
      references Employee (EmployeeNumber)
go

alter table EmployeePermission
   add constraint FK_EMPLOYEE_POE_PERMI_PERMISSI foreign key (PermissionName)
      references Permission (PermissionName)
go

alter table EmployeeRole
   add constraint FK_EMPLOYEE_ROE_EMPLO_EMPLOYEE foreign key (EmployeeNumber)
      references Employee (EmployeeNumber)
go

alter table EmployeeRole
   add constraint FK_EMPLOYEE_REFERENCE_ROLE foreign key (RoleName)
      references Role (RoleName)
go

alter table Filmcheque
   add constraint FK_EMPLOYEE_blocked_FILMCHEQUE foreign key (BlockedByEmployeeNumber)
      references Employee (EmployeeNumber)
go

alter table Filmcheque
   add constraint FK_FILMCHEQ_FILMCHEQU_BLOCKREA foreign key (BlockReason)
      references BlockReasons (Reason)
go

alter table Filmcheque
   add constraint FK_FILMCHEQUE_of_ONLINE_ORDER foreign key (OrderNumber)
      references Onlineorder (OrderNumber)
go

alter table Filmcheque
   add constraint FK_PRINTER_printed_FILMCHEQUE foreign key (SalePrinterNumber)
      references Printer (PrinterNumber)
go

alter table Filmcheque
   add constraint FK_SALESPOINT_sold_FILMCHEQUE foreign key (CinemaNumber, SalespointNumber)
      references Salespoint (CinemaNumber, SalespointNumber)
go

alter table Filmcheque
   add constraint FK_FILMCHEQUE_printed_at_WORKPLACE foreign key (PrintWorkplaceNumber)
      references Workplace (WorkplaceNumber)
go

alter table Filmcheque
   add constraint FK_FILMCHEQUE_is_sold_at_WORKPLACE foreign key (SaleWorkplaceNumber)
      references Workplace (WorkplaceNumber)
go

alter table Onlineorder
   add constraint FK_BILLING_ADDRESS_of_ONLINE_ORDER foreign key (ShippingAddressNumber)
      references Address (AddressNumber)
go

alter table Onlineorder
   add constraint FK_ADDRESS_of_ONLINE_ORDER foreign key (BillingAddressNumber)
      references Address (AddressNumber)
go

alter table Performance
   add constraint FK_PERFORMANCE_of_FILM foreign key (FilmNumber)
      references Film (FilmNumber)
go

alter table Performance
   add constraint FK_PERFORMANCE_is_shown_in_ROOM foreign key (PerformanceRoomNumber)
      references Room (RoomNumber)
go

alter table Performanceofcinema
   add constraint FK_PERFORMANCE_in_CINEMA foreign key (FilmNumber, PerformanceOfCinemaNumber)
      references Performance (FilmNumber, PerformancePerformanceNumber)
go

alter table Performanceofcinema
   add constraint FK_PERFORMANCE_in_CINEMA2 foreign key (CinemaNumber)
      references Cinema (CinemaNumber)
go

alter table PermissionOfRole
   add constraint FK_PERMISSI_PERMISSIO_PERMISSI foreign key (PermissionName)
      references Permission (PermissionName)
go

alter table PermissionOfRole
   add constraint FK_PERMISSI_REFERENCE_ROLE foreign key (RoleName)
      references Role (RoleName)
go

alter table Product
   add constraint FK_PRODUCT_of_PRODUCTTYPE foreign key (ProductTypeName)
      references Producttype (ProductTypeName)
go

alter table Product
   add constraint FK_PRODUCT_PRODUCT_O_FILMCHEQ foreign key (FilmchequeNumber)
      references Filmcheque (FilmchequeNumber)
go

alter table Product
   add constraint FK_PRODUCT_ROOM_OF_P_ROOM foreign key (RoomNumber)
      references Room (RoomNumber)
go

alter table Producttype
   add constraint FK_FILM_of_PRODUCTTYPE foreign key (FilmNumber)
      references Film (FilmNumber)
go

alter table Producttypeprice
   add constraint FK_PRODUCTTYPE_PRICE_belongs_to_CINEMA foreign key (CinemaNumber)
      references Cinema (CinemaNumber)
go

alter table Producttypeprice
   add constraint FK_PRODUCTTYPE_PRICE_of_PRODUCTTYPE foreign key (ProductTypeName)
      references Producttype (ProductTypeName)
go

alter table Producttypesvansalespoint
   add constraint FK_PRODUCTT_PRODUCTTY_PRODUCTT foreign key (ProductypesOfSaleProducttypeName)
      references Producttype (ProductTypeName)
go

alter table Producttypesvansalespoint
   add constraint FK_PRODUCTT_PRODUCTTY_SALESPOI foreign key (CinemaNumber, SalespointNumber)
      references Salespoint (CinemaNumber, SalespointNumber)
go

alter table Room
   add constraint FK_ROOM_in_CINEMA foreign key (CinemaNumber)
      references Cinema (CinemaNumber)
go

alter table Salespoint
   add constraint FK_EMPLOYEE_manages_SALESPOINT foreign key (EmployeeNumber)
      references Employee (EmployeeNumber)
go

alter table Salespoint
   add constraint FK_SALESPOINT_of_CINEMA foreign key (CinemaNumber)
      references Cinema (CinemaNumber)
go

alter table Workplace
   add constraint FK_EMPLOYEE_works_at_WORKPLACE foreign key (EmployeeNumber)
      references Employee (EmployeeNumber)
go

alter table Workplace
   add constraint FK_WORKPLACE_of_SALESPOINT foreign key (CinemaNumber, SalespointNumber)
      references Salespoint (CinemaNumber, SalespointNumber)
go

