CREATE or ALTER PROCEDURE [sp_add_hist_trigger]
	@table varchar(255)
AS
	SET NOCOUNT ON 
	SET XACT_ABORT OFF
	DECLARE @TranCounter INT;
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0 
		SAVE TRANSACTION ProcedureSave;
	ELSE 
		BEGIN TRANSACTION;
	BEGIN TRY
	    	--get primary key columns
		declare @columns nvarchar(max) = ''
		SELECT @columns = @columns + COLUMN_NAME+','
		FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
		WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA + '.' + QUOTENAME(CONSTRAINT_NAME)), 'IsPrimaryKey') = 1
		AND TABLE_NAME = @table

		--create exact copy of the table without constraints
		declare @sql nvarchar(MAX) = ''
		set @sql = 'SELECT top 0 * INTO HIST_'+@table+' FROM '+@table + ' union all select * from ' +@table +' where 1 = 0'
		exec sp_sqlexec @sql

		--add timestamp column
		set @sql = 'ALTER TABLE HIST_'+@table+' ADD [timestamp] datetime default getdate() not null'
		exec sp_sqlexec @sql
		
		--add timestamp column as primary key column
		set @columns = @columns + 'timestamp'

		--add primary key
		set @sql = 'ALTER TABLE HIST_'+@table+' ADD  PRIMARY KEY ('+@columns+');'
		exec sp_sqlexec @sql
	
		--see columns to transfer
		declare @Histcolumns nvarchar(max) =''
		SELECT @Histcolumns = @Histcolumns+ COLUMN_NAME+', ' 
		FROM INFORMATION_SCHEMA.COLUMNS
		WHERE TABLE_NAME = @table
		set @Histcolumns = SUBSTRING(@Histcolumns,0,LEN(@Histcolumns))

		--update
		set @sql ='create or alter trigger update_hist_'+@table+' on '+@table+' for update as 
					set nocount on 
					insert into HIST_'+@table+' ('+@Histcolumns+') select '+@Histcolumns+' from deleted;'
		exec sp_sqlexec @sql
		set @sql = 'exec sp_settriggerorder @triggername = '''+concat('update_hist_',@table)+''', @order = ''last'', @stmttype = ''update'''
		exec sp_sqlexec @sql

		--delete
		set @sql ='create or alter trigger delete_hist_'+@table+' on '+@table+' for delete as 
					set nocount on 
					insert into HIST_'+@table+' ('+@Histcolumns+') select '+@Histcolumns+' from deleted;'
		exec sp_sqlexec @sql	
		set @sql = 'exec sp_settriggerorder @triggername = '''+concat('delete_hist_',@table)+''', @order = ''last'', @stmttype = ''delete'''
		exec sp_sqlexec @sql

		IF @TranCounter = 0 AND XACT_STATE() = 1
			COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		IF @TranCounter = 0 
			BEGIN
				IF XACT_STATE() = 1 ROLLBACK TRANSACTION;
			END;
		ELSE
			BEGIN
				IF XACT_STATE() <> -1 ROLLBACK TRANSACTION ProcedureSave;
			END;	
		THROW
	END CATCH
GO


CREATE or alter PROCEDURE [sp_create_hist_tables]
AS
	SET NOCOUNT ON 
	SET XACT_ABORT OFF
	DECLARE @TranCounter INT;
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0 
		SAVE TRANSACTION ProcedureSave;
	ELSE 
		BEGIN TRANSACTION;
	BEGIN TRY		
		declare @sql nvarchar(max) = ''

		select @sql = @sql +'exec [sp_add_hist_trigger] ' +i.[TABLE_NAME]+';' from INFORMATION_SCHEMA.TABLES i
		where 'HIST_'+i.TABLE_NAME not in (select TABLE_NAME from INFORMATION_SCHEMA.TABLES)
		and i.[TABLE_NAME] not like 'HIST_%' AND i.TABLE_SCHEMA='dbo';
		
		exec sp_sqlexec @sql
		
		IF @TranCounter = 0 AND XACT_STATE() = 1
			COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		IF @TranCounter = 0 
			BEGIN
				IF XACT_STATE() = 1 ROLLBACK TRANSACTION;
			END;
		ELSE
			BEGIN
				IF XACT_STATE() <> -1 ROLLBACK TRANSACTION ProcedureSave;
			END;	
		THROW
	END CATCH
GO

exec sp_create_hist_tables

