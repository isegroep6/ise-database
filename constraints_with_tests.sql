/***********************************************
*  CONSTRAINT 1                                *
***********************************************/
-- Block_reason is null als de filmcheque niet geblokkeerd is, als Block_reason wel een waarde heeft dan is de filmcheque geblokkeerd.

alter table filmcheque
    add constraint CK_BlockReason check ((printstatus != 'blocked' and blockreason is null) or
                                         (printstatus = 'blocked' and blockreason is not null))

/***********************************************
*  CONSTRAINT 1  >> Tests                      *
***********************************************/
EXEC tSQLt.NewTestClass 'Constraint1';
/***********************************************
*  CONSTRAINT 1  >> Tests >> Test 1            *
***********************************************/
GO
CREATE PROCEDURE [Constraint1].[test 1 =  no print status]
AS
BEGIN
    EXEC tSQLt.FakeTable 'dbo.filmcheque';

    EXEC tSQLt.ApplyConstraint @SchemaName= 'dbo', @Tablename = 'filmcheque', @ConstraintName = 'CK_BlockReason';

    EXEC tSQLt.ExpectNoException

    insert into filmcheque (PrintStatus, BlockReason)
    values (null, null)

END
GO
/***********************************************
*  CONSTRAINT 1  >> Tests >> Test 2            *
***********************************************/
CREATE PROCEDURE [Constraint1].[test 2 =  blocked with message]
AS
BEGIN
    EXEC tSQLt.FakeTable 'dbo.filmcheque';

    EXEC tSQLt.ApplyConstraint @SchemaName= 'dbo', @Tablename = 'filmcheque', @ConstraintName = 'CK_BlockReason';

    EXEC tSQLt.ExpectNoException

    insert into filmcheque (PrintStatus, BlockReason)
    values ('blocked', 'test')

END
GO
/***********************************************
*  CONSTRAINT 1  >> Tests >> Test 3            *
***********************************************/
CREATE PROCEDURE [Constraint1].[test 3 =  blocked without message]
AS
BEGIN
    EXEC tSQLt.FakeTable 'dbo.filmcheque';

    EXEC tSQLt.ApplyConstraint @SchemaName= 'dbo', @Tablename = 'filmcheque', @ConstraintName = 'CK_BlockReason';

    EXEC tSQLt.ExpectException
         'The INSERT statement conflicted with the CHECK constraint "CK_BlockReason". The conflict occurred in database "filmcheques", table "dbo.Filmcheque".'

    insert into filmcheque (PrintStatus, BlockReason)
    values ('blocked', null)

END
GO
/***********************************************
*  CONSTRAINT 1  >> Tests >> Test 4            *
***********************************************/
CREATE PROCEDURE [Constraint1].[test 4 =  sold with a block reason]
AS
BEGIN
    EXEC tSQLt.FakeTable 'dbo.filmcheque';

    EXEC tSQLt.ApplyConstraint @SchemaName= 'dbo', @Tablename = 'filmcheque', @ConstraintName = 'CK_BlockReason';

    EXEC tSQLt.ExpectException
         'The INSERT statement conflicted with the CHECK constraint "CK_BlockReason". The conflict occurred in database "filmcheques", table "dbo.Filmcheque".'

    insert into filmcheque (PrintStatus, BlockReason)
    values ('sold', 'test')

END
GO


EXEC [tSQLt].[Run] 'Constraint1'


/***********************************************
*  CONSTRAINT 2                                *
***********************************************/
-- Print_status is standaard “sold”. Andere waardes voor Print_status zijn “printed”, “returned” of “blocked”.

alter table filmcheque
    add constraint CK_PrintStatus check (PrintStatus in ('sold', 'printed', 'blocked', 'returned'))

/***********************************************
*  CONSTRAINT 2  >> Tests                      *
***********************************************/
EXEC tSQLt.NewTestClass 'Constraint2';
/***********************************************
*  CONSTRAINT 2  >> Tests >> Test 1            *
***********************************************/
GO
CREATE PROCEDURE [Constraint2].[test = 1 print status: not assigned]
AS
BEGIN
    EXEC tSQLt.FakeTable 'dbo.filmcheque';

    EXEC tSQLt.ApplyConstraint @SchemaName= 'dbo', @Tablename = 'filmcheque', @ConstraintName = 'CK_PrintStatus';

    EXEC tSQLt.ExpectNoException

    insert into filmcheque (PrintStatus)
    values (null)

END
GO
/***********************************************
*  CONSTRAINT 2  >> Tests >> Test 2            *
***********************************************/
CREATE PROCEDURE [Constraint2].[test = 2 print status: sold]
AS
BEGIN
    EXEC tSQLt.FakeTable 'dbo.filmcheque';

    EXEC tSQLt.ApplyConstraint @SchemaName= 'dbo', @Tablename = 'filmcheque', @ConstraintName = 'CK_PrintStatus';

    EXEC tSQLt.ExpectNoException

    insert into filmcheque (PrintStatus)
    values ('sold')

END
GO
/***********************************************
*  CONSTRAINT 2  >> Tests >> Test 3            *
***********************************************/
CREATE PROCEDURE [Constraint2].[test = 3 print status: printed]
AS
BEGIN
    EXEC tSQLt.FakeTable 'dbo.filmcheque';

    EXEC tSQLt.ApplyConstraint @SchemaName= 'dbo', @Tablename = 'filmcheque', @ConstraintName = 'CK_PrintStatus';

    EXEC tSQLt.ExpectNoException

    insert into filmcheque (PrintStatus)
    values ('printed')

END
GO
/***********************************************
*  CONSTRAINT 2  >> Tests >> Test 4            *
***********************************************/
CREATE PROCEDURE [Constraint2].[test = 4 print status: blocked]
AS
BEGIN
    EXEC tSQLt.FakeTable 'dbo.filmcheque';

    EXEC tSQLt.ApplyConstraint @SchemaName= 'dbo', @Tablename = 'filmcheque', @ConstraintName = 'CK_PrintStatus';

    EXEC tSQLt.ExpectNoException

    insert into filmcheque (PrintStatus)
    values ('blocked')

END
GO
/***********************************************
*  CONSTRAINT 2  >> Tests >> Test 5            *
***********************************************/
CREATE PROCEDURE [Constraint2].[test = 5 print status: returned]
AS
BEGIN
    EXEC tSQLt.FakeTable 'dbo.filmcheque';

    EXEC tSQLt.ApplyConstraint @SchemaName= 'dbo', @Tablename = 'filmcheque', @ConstraintName = 'CK_PrintStatus';

    EXEC tSQLt.ExpectNoException

    insert into filmcheque (PrintStatus)
    values ('returned')

END
GO
/***********************************************
*  CONSTRAINT 2  >> Tests >> Test 6            *
***********************************************/
CREATE PROCEDURE [Constraint2].[test = 6 print status: not allowed]
AS
BEGIN
    EXEC tSQLt.FakeTable 'dbo.filmcheque';

    EXEC tSQLt.ApplyConstraint @SchemaName= 'dbo', @Tablename = 'filmcheque', @ConstraintName = 'CK_PrintStatus';

    EXEC tSQLt.ExpectException
         'The INSERT statement conflicted with the CHECK constraint "CK_PrintStatus". The conflict occurred in database "filmcheques", table "dbo.Filmcheque", column ''PrintStatus''.'

    insert into filmcheque (PrintStatus)
    values ('anders')

END
GO
/***********************************************
*  CONSTRAINT 4                                *
***********************************************/
-- Bij een online bestelling mogen maximaal 10 filmcheques worden gekocht.
CREATE trigger utr_maximum_filmcheques_in_order
    on Filmcheque
    after insert, update
    AS
    BEGIN TRY
        --If the new OrderNumber of this Filmcheque row is present in 10 or more other filmcheques
        if EXISTS(SELECT 1
                  FROM Filmcheque
                  WHERE OrderNumber in (select OrderNumber from inserted)
                  GROUP BY OrderNumber
                  HAVING COUNT(*) > 10)
            THROW 50003, 'There are already 10 or more filmcheques in this order', 1;
    END TRY
    BEGIN CATCH
        ; THROW
    END CATCH
go
/***********************************************
*  CONSTRAINT 4  >> Tests                      *
***********************************************/
exec tSQLt.NewTestClass 'Constraint 4'
/***********************************************
*  CONSTRAINT 4  >> Tests >> Test 1            *
***********************************************/
drop procedure if exists [Constraint 4].[test 1 = 11 filmcheques in order zodat trigger afgaat]
go
create procedure [Constraint 4].[test 1 = 11 filmcheques in order zodat trigger afgaat]
as
begin
    exec tSQLt.FakeTable 'dbo.Filmcheque'

    exec [tSQLt].[ApplyTrigger] @tablename = 'dbo.Filmcheque', @triggername = 'utr_maximum_filmcheques_in_order'

    exec tSQLt.ExpectException 'There are already 10 or more filmcheques in this order'

    insert into Filmcheque (OrderNumber)
    values (66),
           (66),
           (66),
           (66),
           (66),
           (66),
           (66),
           (66),
           (66),
           (66),
           (66),
           (66),
           (66),
           (66),
           (66)

end
go
/***********************************************
*  CONSTRAINT 4  >> Tests >> Test 2            *
***********************************************/
drop procedure if exists [Constraint 4].[test 2 = order nummer updaten zodat de trigger afgaat]
go
create procedure [Constraint 4].[test 2 = order nummer updaten zodat de trigger afgaat]
as
begin
    exec tSQLt.FakeTable 'dbo.Filmcheque'

    exec [tSQLt].[ApplyTrigger] @tablename = 'dbo.Filmcheque', @triggername = 'utr_maximum_filmcheques_in_order'

    exec tSQLt.ExpectException 'There are already 10 or more filmcheques in this order'

    insert into Filmcheque (OrderNumber)
    values (66),
           (66),
           (66),
           (66),
           (66),
           (66),
           (66),
           (66),
           (67),
           (67),
           (67)

    update Filmcheque set OrderNumber = 66
end
go

/***********************************************
*  CONSTRAINT 4  >> Tests >> Test 3            *
***********************************************/
drop procedure if exists [Constraint 4].[test 3 = succesvol inserten en updaten]
go
create procedure [Constraint 4].[test 3 = succesvol inserten en updaten]
as
begin
    exec tSQLt.FakeTable 'dbo.filmcheque';

    insert into Filmcheque (OrderNumber)
    values (66),
           (66),
           (66),
           (66),
           (66),
           (66),
           (66),
           (66),
           (67),
           (67),
           (67)

    exec [tSQLt].[ApplyTrigger] @tablename = 'dbo.filmcheque', @triggername = 'utr_maximum_filmcheques_in_order'

    exec tSQLt.ExpectNoException

    update Filmcheque set OrderNumber = 66 where FilmchequeNumber = 9
end
go

/***********************************************
*  CONSTRAINT 4  >> Tests >> Test 4            *
***********************************************/
drop procedure if exists [Constraint 4].[test 4 = inserten voor meerdere bestellingen]
go
create procedure [Constraint 4].[test 4 = inserten voor meerdere bestellingen]
as
begin
    exec tSQLt.FakeTable 'dbo.filmcheque';

    exec [tSQLt].[ApplyTrigger] @tablename = 'dbo.filmcheque', @triggername = 'utr_maximum_filmcheques_in_order'

    exec tSQLt.ExpectException 'There are already 10 or more filmcheques in this order'

    insert into Filmcheque (OrderNumber)
    values (66),
           (66),
           (66),
           (66),
           (66),
           (66),
           (66),
           (66),
           (67),
           (67),
           (67)

    insert into Filmcheque (OrderNumber)
    values (66),
           (66),
           (66),
           (67)
end
go

/***********************************************
*  CONSTRAINT 4  >> Tests >> Test 5            *
***********************************************/
drop procedure if exists [Constraint 4].[test = 5 te veel inserten voor meerdere bestellingen]
go
create procedure [Constraint 4].[test 5 = te veel inserten voor meerdere bestellingen]
as
begin
    exec tSQLt.FakeTable 'dbo.filmcheque';

    exec [tSQLt].[ApplyTrigger] @tablename = 'dbo.filmcheque', @triggername = 'utr_maximum_filmcheques_in_order'

    exec tSQLt.ExpectException 'There are already 10 or more filmcheques in this order'

    insert into Filmcheque (OrderNumber)
    values (66),
           (66),
           (66),
           (66),
           (66),
           (66),
           (66),
           (66),
           (66),
           (66),
           (66),
           (66),
           (66),
           (67),
           (67),
           (67),
           (67),
           (67),
           (67),
           (67),
           (67),
           (67),
           (67),
           (67),
           (67),
           (67)

end
go


/***********************************************
*  CONSTRAINT 5                                *
***********************************************/
-- De prijs van een product heeft een waarde van minimaal 5 als het producttype een vrij te besteden bedrag is.
CREATE trigger utr_minimal_price_for_free_product
    on product
    after insert, update
    AS
    BEGIN TRY

        --if the price in the updated/inserted row is less than 5
        if ((select count(*) from inserted where ProducttypeName = 'Vrij te besteden bedrag' and Price < 5) > 0)
            THROW 50001, 'The productprice for a product of type Vrij te besteden bedrag cant be set to a value lower than 5', 1;

        if ((select count(*) from inserted where ProducttypeName = 'Vrij te besteden bedrag' and (Price % 2.5) > 0) > 0)
            THROW 50001, 'The productprice for a product of type Vrij te besteden bedrag can only be in increases of 2.50', 1;

    END TRY
    BEGIN CATCH
        ; THROW
    END CATCH
go
/***********************************************
*  CONSTRAINT 5  >> Tests                      *
***********************************************/
EXEC tSQLt.NewTestClass 'Constraint5';
GO
/***********************************************
*  CONSTRAINT 5  >> Tests >> Test 1            *
***********************************************/
CREATE PROCEDURE [Constraint5].[test = 1 filmcheque without free spending]
AS
BEGIN
    EXEC tSQLt.FakeTable 'dbo.product';

    EXEC [tSQLt].[ApplyTrigger] @tablename = 'dbo.product', @triggername = 'utr_minimal_price_for_free_product'

    EXEC tSQLt.ExpectNoException

    insert into product (ProductTypeName, Price)
    values ('niet vrij te besteden', null)

END
GO
/***********************************************
*  CONSTRAINT 5  >> Tests >> Test 2            *
***********************************************/
CREATE PROCEDURE [Constraint5].[test = 2 filmcheque with minimal value]
AS
BEGIN
    EXEC tSQLt.FakeTable 'dbo.product';

    EXEC [tSQLt].[ApplyTrigger] @tablename = 'dbo.product', @triggername = 'utr_minimal_price_for_free_product'

    EXEC tSQLt.ExpectNoException

    insert into product (ProductTypeName, Price)
    values ('niet vrij te besteden', 5)

END
GO
/***********************************************
*  CONSTRAINT 5  >> Tests >> Test 3            *
***********************************************/
CREATE PROCEDURE [Constraint5].[test = 3 filmcheque value is a multiple of 2.50]
AS
BEGIN
    EXEC tSQLt.FakeTable 'dbo.product';

    EXEC [tSQLt].[ApplyTrigger] @tablename = 'dbo.product', @triggername = 'utr_minimal_price_for_free_product'

    EXEC tSQLt.ExpectNoException

    insert into product (ProductTypeName, Price)
    values ('niet vrij te besteden', 12.50)

END
GO
/***********************************************
*  CONSTRAINT 5  >> Tests >> Test 4            *
***********************************************/
CREATE PROCEDURE [Constraint5].[test = 4 filmcheque value below the minimum]
AS
BEGIN
    EXEC tSQLt.FakeTable 'dbo.product';

    EXEC [tSQLt].[ApplyTrigger] @tablename = 'dbo.product', @triggername = 'utr_minimal_price_for_free_product'

    EXEC tSQLt.ExpectException
         'The productprice for a product of type Vrij te besteden bedrag cant be set to a value lower than 5'

    insert into product (ProductTypeName, Price)
    values ('Vrij te besteden bedrag', 3)
END
GO
/***********************************************
*  CONSTRAINT 5  >> Tests >> Test 5            *
***********************************************/
CREATE PROCEDURE [Constraint5].[test = 5 filmcheque value is not a multiple of 2.50]
AS
BEGIN
    EXEC tSQLt.FakeTable 'dbo.product';

    EXEC [tSQLt].[ApplyTrigger] @tablename = 'dbo.product', @triggername = 'utr_minimal_price_for_free_product'

    EXEC tSQLt.ExpectException
         'The productprice for a product of type Vrij te besteden bedrag can only be in increases of 2.50'

    insert into product (ProductTypeName, Price)
    values ('Vrij te besteden bedrag', 9.95)

END
GO
/***********************************************
*  CONSTRAINT 5  >> Tests >> Test 6            *
***********************************************/
CREATE PROCEDURE [Constraint5].[test = 6 filmcheque value changed to a value that isn't a multiple of 2.50]
AS
BEGIN
    EXEC tSQLt.FakeTable 'dbo.product';
    insert into product (ProductTypeName, Price)
    values ('Vrij te besteden bedrag', 10)

    EXEC [tSQLt].[ApplyTrigger] @tablename = 'dbo.product', @triggername = 'utr_minimal_price_for_free_product'

    EXEC tSQLt.ExpectException
         'The productprice for a product of type Vrij te besteden bedrag can only be in increases of 2.50'

    update product set Price = 11

END
GO
/***********************************************
*  CONSTRAINT 5  >> Tests >> Test 7            *
***********************************************/
CREATE PROCEDURE [Constraint5].[test = 7 filmcheque changed below minimum value]
AS
BEGIN
    EXEC tSQLt.FakeTable 'dbo.product';
    insert into product (ProductTypeName, Price)
    values ('Vrij te besteden bedrag', 10),
           ('Vrij te besteden bedrag', 12)

    EXEC [tSQLt].[ApplyTrigger] @tablename = 'dbo.product', @triggername = 'utr_minimal_price_for_free_product'

    EXEC tSQLt.ExpectException
         'The productprice for a product of type Vrij te besteden bedrag cant be set to a value lower than 5'

    update product set Price = 3

END
GO

EXEC [tSQLt].[Run] 'Constraint5'
go

/***********************************************
*  CONSTRAINT 6                                *
***********************************************/
-- De price is 0 als de naam van het type “Vrij te besteden bedrag” is.
CREATE trigger utr_maximum_producttypeprice_for_free_product
    on ProductTypePrice
    after insert, update
    AS
    BEGIN TRY

        begin
            --if the price in the updated/inserted row is greater than 0 and the producttype is free sum
            if ((select Price from inserted where ProducttypeName = 'Vrij te besteden bedrag') > 0)
                THROW 50002, 'The producttypeprice for a ''Vrij te besteden bedrag'' producttype cant be set to a value higher than 0', 1;
        end

    END TRY
    BEGIN CATCH
        ; THROW
    END CATCH
go

/***********************************************
*  CONSTRAINT 6  >> Tests                      *
***********************************************/
exec tSQLt.NewTestClass 'Constraint 6'
go
/***********************************************
*  CONSTRAINT 6  >> Tests >> Test 1            *
***********************************************/
create procedure [Constraint 6].[test 1 = vrij te besteden zonder bedrag]
as
begin
    exec tSQLt.FakeTable 'dbo.Producttypeprice'

    exec [tSQLt].[ApplyTrigger] @tablename = 'dbo.producttypeprice',
         @triggername = 'utr_maximum_producttypeprice_for_free_product'

    exec tSQLt.ExpectNoException

    insert into Producttypeprice values ('Vrij te besteden bedrag', null, 0)
end
go

/***********************************************
*  CONSTRAINT 6  >> Tests >> Test 2            *
***********************************************/
create procedure [Constraint 6].[test 2 = vrij te besteden met bedrag]
as
begin
    exec tSQLt.FakeTable 'dbo.Producttypeprice'

    exec [tSQLt].[ApplyTrigger] @tablename = 'dbo.producttypeprice',
         @triggername = 'utr_maximum_producttypeprice_for_free_product'

    exec tSQLt.ExpectException
         'The producttypeprice for a ''Vrij te besteden bedrag'' producttype cant be set to a value higher than 0'

    insert into Producttypeprice values ('Vrij te besteden bedrag', null, 1)
end
go

/***********************************************
*  CONSTRAINT 6  >> Tests >> Test 3            *
***********************************************/
create procedure [Constraint 6].[test 3 = ander product type met bedrag]
as
begin
    exec tSQLt.FakeTable 'dbo.Producttypeprice'

    exec [tSQLt].[ApplyTrigger] @tablename = 'dbo.producttypeprice',
         @triggername = 'utr_maximum_producttypeprice_for_free_product'

    exec tSQLt.ExpectNoException

    insert into Producttypeprice values ('ander type', null, 1)
end
go


EXEC [tSQLt].[Run] 'Constraint 6'
GO
/***********************************************
*  CONSTRAINT 7                                *
***********************************************/
-- 7. valid_until moet bij het aanmaken vandaag of na vandaag zijn

CREATE TRIGGER utr_minimal_date_for_valid_until
    ON Filmcheque
    AFTER INSERT, UPDATE
    AS
    BEGIN TRY
        -- if the inserted date is before today, throw
        IF EXISTS(SELECT 1 FROM inserted WHERE ValidUntil < GETDATE())
            THROW 50001, 'A newly inserted validation date cannot preceed the current date.', 1
    END TRY
    BEGIN CATCH
        THROW
    END CATCH
GO
/***********************************************
*  CONSTRAINT 7  >> Tests                      *
***********************************************/
EXEC tSQLt.NewTestClass 'Constraint7';
GO
/***********************************************
*  CONSTRAINT 7  >> Tests >> Test 1            *
***********************************************/
CREATE PROCEDURE [Constraint7].[test = 1 filmcheque with valid date today, multiple records]
AS
BEGIN
    EXEC tSQLt.FakeTable 'dbo.filmcheque'

    EXEC [tSQLt].[ApplyTrigger] @tablename = 'dbo.filmcheque', @triggername = 'utr_minimal_date_for_valid_until'

    EXEC tSQLt.ExpectNoException

    insert into filmcheque (ValidUntil)
    values ((DATEADD(DAY, 1, GETDATE())))

END
GO
/***********************************************
*  CONSTRAINT 7  >> Tests >> Test 2            *
***********************************************/
CREATE PROCEDURE [Constraint7].[test = 2 filmcheque with invalid date yesterday]
AS
BEGIN
    EXEC tSQLt.FakeTable 'dbo.filmcheque'

    EXEC [tSQLt].[ApplyTrigger] @tablename = 'dbo.filmcheque', @triggername = 'utr_minimal_date_for_valid_until'

    EXEC tSQLt.ExpectException 'A newly inserted validation date cannot preceed the current date.'

    insert into filmcheque (ValidUntil)
    values (DATEADD(DAY, -1, GETDATE()))

END
GO
/***********************************************
*  CONSTRAINT 7  >> Tests >> Test 3            *
***********************************************/
CREATE PROCEDURE [Constraint7].[test = 3 multiple correct filmcheques, one filmcheque with valid date yesterday]
AS
BEGIN
    EXEC tSQLt.FakeTable 'dbo.filmcheque'

    EXEC [tSQLt].[ApplyTrigger] @tablename = 'dbo.filmcheque', @triggername = 'utr_minimal_date_for_valid_until'

    EXEC tSQLt.ExpectException 'A newly inserted validation date cannot preceed the current date.'

    insert into filmcheque (ValidUntil)
    values (GETDATE()),
           (GETDATE()),
           (GETDATE()),
           (GETDATE()),
           (DATEADD(DAY, -1, GETDATE())),
           (GETDATE()),
           (GETDATE())

END
GO

/***********************************************
*  CONSTRAINT 8                                *
***********************************************/
create trigger filmchequeConstantCollumns
    on filmcheque
    after update
    as
begin
    if exists(select 1
              from inserted i
                       join deleted d on i.FilmchequeNumber = d.FilmchequeNumber
              where i.CinemaNumber != d.CinemaNumber
                 or i.SalespointNumber != d.SalespointNumber)
        THROW 50001 , 'CinemaNumber and SalespointNumber can''t be modified.',1
end
go

/***********************************************
*  CONSTRAINT 8  >> Tests                      *
***********************************************/
EXEC tSQLt.NewTestClass 'Constraint8';
GO
/***********************************************
*  CONSTRAINT 8  >> Tests >> Test 1            *
***********************************************/
CREATE PROCEDURE [Constraint8].[test 1 = wijzigen van ordernummer]
AS
BEGIN
    EXEC tSQLt.FakeTable 'dbo.filmcheque'

    insert into filmcheque (filmchequenumber, CinemaNumber, SalespointNumber)
    values (1, 1, 1),
           (1, 1, 1),
           (1, 1, 1)

    EXEC [tSQLt].[ApplyTrigger] @tablename = 'dbo.filmcheque', @triggername = 'filmchequeConstantCollumns'

    EXEC tSQLt.ExpectNoException

    update Filmcheque set OrderNumber = 10

END
GO
/***********************************************
*  CONSTRAINT 8  >> Tests >> Test 2            *
***********************************************/
CREATE PROCEDURE [Constraint8].[test 2 = wijzigen van cinemanumber]
AS
BEGIN
    EXEC tSQLt.FakeTable 'dbo.filmcheque'

    insert into filmcheque (filmchequenumber, CinemaNumber, SalespointNumber)
    values (1, 1, 1),
           (1, 1, 1),
           (1, 1, 1)

    EXEC [tSQLt].[ApplyTrigger] @tablename = 'dbo.filmcheque', @triggername = 'filmchequeConstantCollumns'

    EXEC tSQLt.ExpectException 'CinemaNumber and SalespointNumber can''t be modified.'

    update Filmcheque set CinemaNumber = 10

END
GO
/***********************************************
*  CONSTRAINT 8  >> Tests >> Test 3            *
***********************************************/
CREATE PROCEDURE [Constraint8].[test 3 = wijzigen van salespointnumber]
AS
BEGIN
    EXEC tSQLt.FakeTable 'dbo.filmcheque'

    insert into filmcheque (filmchequenumber, CinemaNumber, SalespointNumber)
    values (1, 1, 1),
           (1, 1, 1),
           (1, 1, 1)

    EXEC [tSQLt].[ApplyTrigger] @tablename = 'dbo.filmcheque', @triggername = 'filmchequeConstantCollumns'

    EXEC tSQLt.ExpectException 'CinemaNumber and SalespointNumber can''t be modified.'

    update Filmcheque set SalespointNumber = 10

END
GO

/***********************************************
*  CONSTRAINT 11                               *
***********************************************/
-- Een employee met de status "Terminated" mag geen nieuwe filmcheque aanmaken
create trigger employeeTerminatedSellFilmcheque
    on filmcheque
    after insert
    as
begin
    begin try
        DECLARE
            @inValidRows INT;
        SELECT @inValidRows = COUNT(*)
        FROM Employee e
                 INNER JOIN Workplace w
                            ON w.EmployeeNumber = e.EmployeeNumber
                 INNER JOIN inserted i
                            ON i.SaleWorkplaceNumber = w.WorkplaceNumber
        WHERE e.Status = 'Terminated'
        if (@inValidRows > 0)
            BEGIN
                ;throw 50001, 'This employee has the status "Terminated" and cannot sell a new filmcheque', 1
            END

    end try
    begin catch
        throw
    end catch
end
go

/***********************************************
*  CONSTRAINT 11  >> Tests                     *
***********************************************/
exec tSQLt.NewTestClass 'Constraint11';
GO
/***********************************************
*  CONSTRAINT 11  >> Tests >> Test 1           *
***********************************************/
drop procedure if exists [Constraint11].[test 1 - filmcheque with terminated employee should give error]
go
create procedure [Constraint11].[test 1 - filmcheque with terminated employee should give error]
as
begin

    exec tSQLt.FakeTable 'dbo.Employee'
    exec tSQLt.FakeTable 'dbo.Workplace'
    exec tSQLt.FakeTable 'dbo.Filmcheque'

    exec [tSQLt].[ApplyTrigger] @tablename = 'dbo.filmcheque', @triggername = 'employeeTerminatedSellFilmcheque'

    exec tSQLt.ExpectException 'This employee has the status "Terminated" and cannot sell a new filmcheque'

    insert into Employee (EmployeeNumber, Email, Password, Status)
    values (3, 'valerie@admin.com', 'joejoe123', 'terminated');

    insert into Workplace (WorkplaceNumber, EmployeeNumber, CinemaNumber, SalespointNumber, WorkplaceName)
    values (1, 3, 1, 1, 'Valeries plekje');

    insert into Filmcheque (FilmchequeNumber, CinemaNumber, SalespointNumber, BlockedByEmployeeNumber, OrderNumber,
                            SaleWorkplaceNumber, SalePrinterNumber, PrintWorkplaceNumber, PrintStatus,
                            ValidMonday, ValidTuesday, ValidWednesday, ValidThursday, ValidFriday,
                            ValidSaturday, ValidSunday, ValidUntil, BlockReason)
    values (1, 1, 1, null, 70, 1, 1, 1, 'sold', 1, 1, 1, 1, 1, 1, 1, '2020-05-23 12:46:53.477', null)

end
go

/***********************************************
*  CONSTRAINT 11  >> Tests >> Test 2           *
***********************************************/
drop procedure if exists [Constraint11].[test 2 - filmcheque with active employee should not give error]
go
create procedure [Constraint11].[test 2 - filmcheque with active employee should not give error]
as
begin

    exec tSQLt.FakeTable 'dbo.Employee'
    exec tSQLt.FakeTable 'dbo.Workplace'
    exec tSQLt.FakeTable 'dbo.Filmcheque'

    exec [tSQLt].[ApplyTrigger] @tablename = 'dbo.filmcheque', @triggername = 'employeeTerminatedSellFilmcheque'

    exec tSQLt.ExpectNoException

    insert into Employee (EmployeeNumber, Email, Password, Status)
    values (3, 'valerie@admin.com', 'joejoe123', 'active');

    insert into Workplace (WorkplaceNumber, EmployeeNumber, CinemaNumber, SalespointNumber, WorkplaceName)
    values (1, 3, 1, 1, 'Valeries plekje');

    insert into Filmcheque (FilmchequeNumber, CinemaNumber, SalespointNumber, BlockedByEmployeeNumber, OrderNumber,
                            SaleWorkplaceNumber, SalePrinterNumber, PrintWorkplaceNumber, PrintStatus,
                            ValidMonday, ValidTuesday, ValidWednesday, ValidThursday, ValidFriday,
                            ValidSaturday, ValidSunday, ValidUntil, BlockReason)
    values (1, 1, 1, null, 70, 1, 1, 1, 'sold', 1, 1, 1, 1, 1, 1, 1, '2020-05-23 12:46:53.477', null)

end
go

/*
-----------------------------------------------------------------------------------------------------------------
*/

/***********************************************
*  CONSTRAINT 12                               *
***********************************************/
alter table filmcheque
    add constraint CK_BlockRemark check (
            (remark is null and blockreason != 'other, see remark') or
            ( remark is not null and blockreason = 'other, see remark')
        )

-- remark mag alleen ingevuld zijn als de blokkeer reden “other, see remark”

/***********************************************
*  CONSTRAINT 12  >> Tests                     *
***********************************************/
EXEC tSQLt.NewTestClass 'Constraint12';
GO
/***********************************************
*  CONSTRAINT 12  >> Tests >> Test 1           *
***********************************************/
CREATE PROCEDURE [Constraint12].[test 1 =  not blocked]
AS
BEGIN
    EXEC tSQLt.FakeTable 'dbo.filmcheque';

    EXEC tSQLt.ApplyConstraint @SchemaName= 'dbo', @Tablename = 'filmcheque', @ConstraintName = 'CK_BlockRemark';

    EXEC tSQLt.ExpectNoException

    insert into filmcheque (PrintStatus, BlockReason)
    values (null, null)

END
GO
/***********************************************
*  CONSTRAINT 12  >> Tests >> Test 2           *
***********************************************/
CREATE PROCEDURE [Constraint12].[test 2 =  missing remark]
AS
BEGIN
    EXEC tSQLt.FakeTable 'dbo.filmcheque';

    EXEC tSQLt.ApplyConstraint @SchemaName= 'dbo', @Tablename = 'filmcheque', @ConstraintName = 'CK_BlockRemark';

    EXEC tSQLt.ExpectException
         'The INSERT statement conflicted with the CHECK constraint "CK_BlockRemark". The conflict occurred in database "filmcheques", table "dbo.Filmcheque".'

    insert into filmcheque (PrintStatus, BlockReason, Remark)
    values (null, 'other, see remark', null)

END
GO
/***********************************************
*  CONSTRAINT 12  >> Tests >> Test 3           *
***********************************************/
CREATE PROCEDURE [Constraint12].[test 3 =  remark when a reason is given]
AS
BEGIN
    EXEC tSQLt.FakeTable 'dbo.BlockReasons';
    EXEC tSQLt.FakeTable 'dbo.filmcheque';

    EXEC tSQLt.ApplyConstraint @SchemaName= 'dbo', @Tablename = 'filmcheque', @ConstraintName = 'CK_BlockRemark';

    EXEC tSQLt.ExpectException 'The INSERT statement conflicted with the CHECK constraint "CK_BlockRemark". The conflict occurred in database "filmcheques", table "dbo.Filmcheque".'

    insert into filmcheque (PrintStatus, BlockReason, Remark)
    values (null, 1, 'reden')

END
GO
/***********************************************
*  CONSTRAINT 12  >> Tests >> Test 4           *
***********************************************/
CREATE PROCEDURE [Constraint12].[test 4 =  remark without blockreason]
AS
BEGIN
    EXEC tSQLt.FakeTable 'dbo.filmcheque';

    EXEC tSQLt.ApplyConstraint @SchemaName= 'dbo', @Tablename = 'filmcheque', @ConstraintName = 'CK_BlockRemark';

    EXEC tSQLt.ExpectNoException

    insert into filmcheque (BlockReason, Remark)
    values (null, 'hierom')

END
GO

EXEC [tSQLt].[RunAll]