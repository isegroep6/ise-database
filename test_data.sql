use filmcheques;

--blockReason
insert into BlockReasons values
('other, see remark'),
('lost in mail'),
('error printing'),
('stolen')

--address
INSERT INTO filmcheques.dbo.Address (Street, PostalCode, Firstname, Lastname, City, Email, CompanyName)
VALUES ('2101 Rafe Lane', '39201', 'Jan', 'Jansen', 'Norcross', 'jan@example.com', 'Transavia'),
('Ruitenberglaan 31', '6826CC', null, null, 'Arnhem', 'han@example.com', 'HAN');

--employee
INSERT INTO filmcheques.dbo.Employee (Email, Password, Status)
VALUES ('admin@admin.com', 'ADeMgCEOp7mdiaroTF3vlvzzm0eBfrqSZGhah+/Ee5nhbKrnZ35tGIChFPDcXImNZA==', 'active');

--cinema
INSERT INTO filmcheques.dbo.Cinema (EmployeeNumber, CinemaName)
VALUES (1, 'tabakzaak');

--salespoint
INSERT INTO filmcheques.dbo.Salespoint (CinemaNumber, EmployeeNumber, Name)
VALUES (1, 1, 'tabak');

--workplace
INSERT INTO filmcheques.dbo.Workplace (EmployeeNumber, CinemaNumber, SalespointNumber, WorkplaceName)
VALUES (1, 1, 1, 'Test');
INSERT INTO filmcheques.dbo.Printer (IpAddress, PrintCode)
VALUES ('1', '1');
INSERT INTO filmcheques.dbo.Onlineorder (ShippingAddressNumber, BIllingAddressNumber, PersonalMessage)
VALUES (1, 1, '1');

--filmcheque
INSERT INTO Filmcheque
(CinemaNumber, SalespointNumber, BlockedByEmployeeNumber, OrderNumber, SaleWorkplaceNumber, SalePrinterNumber,
 PrintWorkplaceNumber, PrintStatus, ValidMonday, ValidTuesday, ValidWednesday, ValidThursday, ValidFriday,
 ValidSaturday, ValidSunday, ValidUntil, BlockReason)
VALUES (1, 1, null, 1, 1, 1, 1, 'sold', 1, 1, 1, 1, 1, 1, 1, '2019-05-23 12:46:53.477', null)

INSERT INTO Filmcheque
(CinemaNumber, SalespointNumber, BlockedByEmployeeNumber, OrderNumber, SaleWorkplaceNumber, SalePrinterNumber,
 PrintWorkplaceNumber, PrintStatus, ValidMonday, ValidTuesday, ValidWednesday, ValidThursday, ValidFriday,
 ValidSaturday, ValidSunday, ValidUntil, BlockReason)
VALUES (1, 1, null, 1, 1, 1, 1, 'sold', 1, 1, 1, 1, 1, 1, 1, '2019-02-23 12:46:53.477', null)

INSERT INTO Filmcheque
(CinemaNumber, SalespointNumber, BlockedByEmployeeNumber, OrderNumber, SaleWorkplaceNumber, SalePrinterNumber,
 PrintWorkplaceNumber, PrintStatus, ValidMonday, ValidTuesday, ValidWednesday, ValidThursday, ValidFriday,
 ValidSaturday, ValidSunday, ValidUntil, BlockReason)
VALUES (1, 1, null, 1, 1, 1, 1, 'sold', 1, 1, 1, 1, 1, 1, 1, '2019-01-23 12:46:53.477', null)

--producttype
INSERT INTO filmcheques.dbo.Producttype (ProductTypeName, FilmNumber) VALUES ('Toegang', null);
INSERT INTO filmcheques.dbo.Producttype (ProductTypeName, FilmNumber) VALUES ('Extra', null);
INSERT INTO filmcheques.dbo.Producttype (ProductTypeName, FilmNumber) VALUES ('Cola', null);

--room
INSERT INTO filmcheques.dbo.Room VALUES(1, 1, 'Zaal 1')
INSERT INTO filmcheques.dbo.Room VALUES(1, 2, 'Zaal 2')

--product
INSERT INTO filmcheques.dbo.Product (ProductTypeName, FilmchequeNumber, RoomNumber, Price, Amount, BarCode) VALUES ('Toegang', 1, 1, 15, 2, '123124');
INSERT INTO filmcheques.dbo.Product (ProductTypeName, FilmchequeNumber, RoomNumber, Price, Amount, BarCode) VALUES ('Extra', 1, 1, 0, 1, '123455');
INSERT INTO filmcheques.dbo.Product (ProductTypeName, FilmchequeNumber, RoomNumber, Price, Amount, BarCode) VALUES ('Cola', 1, 1, 2.5, 1, '57841');

--role
INSERT INTO filmcheques.dbo.Role(RoleName) VALUES ('CreateFilmcheque')
INSERT INTO filmcheques.dbo.Role(RoleName) VALUES ('Admin')

--employeeRole
INSERT INTO filmcheques.dbo.EmployeeRole(EmployeeNumber, RoleName) VALUES (1, 'CreateFilmcheque');
INSERT INTO filmcheques.dbo.EmployeeRole(EmployeeNumber, RoleName) VALUES (1, 'Admin');

/*
---------------------------------------testdata voor testplan------------------------------------------------------------
*/
--employee
INSERT INTO filmcheques.dbo.Employee (Email, Password, Status)
VALUES ('test1@example.com', 'AN6IEu0w+pBbiIdFhuOw4YX15DLpRSHyVy3I3tDrYU8TVF0HXHjhYo514YcN21oC+w==', 'active'),
('test2@example.com', 'AN6IEu0w+pBbiIdFhuOw4YX15DLpRSHyVy3I3tDrYU8TVF0HXHjhYo514YcN21oC+w==', 'active');

--workplace
INSERT INTO filmcheques.dbo.Workplace (EmployeeNumber, CinemaNumber, SalespointNumber, WorkplaceName)
select employeenumber, 1, 1, 'Test'
from employee where email = 'test1@example.com'

INSERT INTO filmcheques.dbo.Workplace (EmployeeNumber, CinemaNumber, SalespointNumber, WorkplaceName)
select employeenumber, 1, 1, 'Test'
from employee where email = 'test2@example.com'

--employeerole
INSERT INTO filmcheques.dbo.EmployeeRole(EmployeeNumber, RoleName) 
select employee.employeenumber, 'CreateFilmcheque'
from employee where email = 'test1@example.com'

INSERT INTO filmcheques.dbo.EmployeeRole(EmployeeNumber, RoleName) 
select employee.employeenumber, 'Admin'
from employee where email = 'test2@example.com'
